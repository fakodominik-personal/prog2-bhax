package hu.unideb.prog2.calculation;

import hu.unideb.prog2.model.BinaryTree;
import hu.unideb.prog2.model.Node;

public class AverageValue {
	
	private static int nodes;
	private static double sum;
	
	public static double getAvg(BinaryTree tree)
	{
		Node temp = tree.getRoot();
		nodes = 0;
		sum = 0;
		return avgRevursive(temp);
	}
	private static double avgRevursive(Node node)  
    { 
        if (node == null) 
            return 0; 
        else 
        { 
            nodes++;
        	sum += node.getValue();
            avgRevursive(node.getLeftChild()); 
            avgRevursive(node.getRightChild()); 
        } 
        
        return sum/(nodes - 1);
    } 
	
}
