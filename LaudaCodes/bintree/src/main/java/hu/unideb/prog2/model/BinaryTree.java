package hu.unideb.prog2.model;

public class BinaryTree {
	private Node root;
	
	public BinaryTree()
	{
		root = new Node(0);
	}
	
	public void add(int value)
	{
		Node current = root;
		Node before = current;
		if(current.getValue() == value)
		{
			error("The value is already in the tree!");
		}else if(value > current.getValue())
		{
			addRightTravelsal(value, current.getRightChild(),before);
		}else if(value < current.getValue())
		{
			addLeftTravelsal(value, current.getLeftChild(),before);
		}
	}
	private void addRightTravelsal(int value,Node node,Node before)
	{
		if(node == null)
		{
			node = new Node(value);
			before.setRightChild(node);
		}else if(node.getValue() == value)
		{
			error("The value is already in the tree!");
		}
		else if(value > node.getValue())
		{
			before = node;
			addRightTravelsal(value, node.getRightChild(),before);
		}else if(value < node.getValue())
		{
			before = node;
			addLeftTravelsal(value, node.getLeftChild(),before);
		}
		
	}
	private void addLeftTravelsal(int value,Node node,Node before)
	{
		if(node == null)
		{
			node = new Node(value);
			before.setLeftChild(node);
		}else if(node.getValue() == value)
		{
			error("The value is already in the tree!");
		}
		else if(value > node.getValue())
		{
			before = node;
			addRightTravelsal(value, node.getRightChild(),before);
		}else if(value < node.getValue())
		{
			before = node;
			addLeftTravelsal(value, node.getLeftChild(),before);
		}
		
	}
	public void  print()
	{
			recursivePrint(root);
	}
	private void recursivePrint(Node node)
	{
		if(node==null)
		{
			return;
		}
			recursivePrint(node.getRightChild());
			recursivePrint(node.getLeftChild());
			System.out.println(node.getValue());
	}
	
	private void error(String str)
	{
		System.out.println(str);
	}
	public Node getRoot()
	{
		return this.root;
	}
}
