package hu.unideb.prog2.calculation;

import java.util.ArrayList;

import hu.unideb.prog2.model.BinaryTree;
import hu.unideb.prog2.model.Node;

public class Deviaton {
	
	private static ArrayList<Double> values;

	public static double getDeviation(double avg,BinaryTree root)
	{
		values = new ArrayList<>();
		getValues(root.getRoot());
		int numberOfElements = values.size();
		values.stream().forEach(a -> a = a - avg);
		values.stream().forEach(a -> a = a * a);
		double sum = values.stream().reduce(0.0,Double::sum);
		
		return Math.sqrt(sum/numberOfElements);
		
	}

	private static void getValues(Node root) {
		if(root==null)
		{
			return;
		}
			getValues(root.getRightChild());
			getValues(root.getLeftChild());
			values.add((double) root.getValue());
	}
	
}
