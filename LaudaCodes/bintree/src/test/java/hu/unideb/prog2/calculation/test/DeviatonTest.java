package hu.unideb.prog2.calculation.test;

import org.junit.Test;
import org.junit.Assert;

import hu.unideb.prog2.calculation.AverageValue;
import hu.unideb.prog2.calculation.Deviaton;
import hu.unideb.prog2.model.BinaryTree;

public class DeviatonTest {

	BinaryTree tree = new BinaryTree();
	
	@Test
	public void testDeviatonShouldReturnTheDeviatonOfTheTree()
	{
		//Given
		tree.add(20);
		tree.add(10);
		tree.add(15);
		tree.add(15);
		double expected = Math.sqrt(11.25);
		
		//When
		double result = Deviaton.getDeviation(AverageValue.getAvg(tree),tree);
		
		//Then
		Assert.assertEquals(expected, result,0);
	}
	
}
