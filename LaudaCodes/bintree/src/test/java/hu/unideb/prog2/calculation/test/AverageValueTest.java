package hu.unideb.prog2.calculation.test;

import org.junit.Assert;
import org.junit.Test;

import hu.unideb.prog2.calculation.AverageValue;
import hu.unideb.prog2.model.BinaryTree;

public class AverageValueTest {

	BinaryTree tree = new BinaryTree();
	@Test
	public void testGetAvgValueShouldReturnTheAverageOfTheValuesInTheTree()
	{
		//Given
		tree.add(3);
		tree.add(10);
		tree.add(20);
		tree.add(31);
		double expected = 16;
		
		//When
		double result = AverageValue.getAvg(tree);
		
		//Then
		Assert.assertEquals(expected, result,0);
	}
	
}
