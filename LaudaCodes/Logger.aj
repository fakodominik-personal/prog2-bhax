public aspect Logger{

	pointcut log() : execution(* LZWBinfa.pushToTree());

	after() : log(){
		System.out.println("Value added to the tree!");
	}
}