package binfa;

public class Csomopont
{
    private Csomopont egyesGyermek;
    private Csomopont nullasGyermek;
    private int value;
    private char betu = '/';

    public Csomopont(int value)
    {
        this.value = value;
        this.egyesGyermek = null;
        this.nullasGyermek = null;
    }
    public Csomopont()
    {
        this.value = 0;
        this.egyesGyermek = null;
        this.nullasGyermek = null;
    }

    public Csomopont getNullasGyermek()
    {
        return this.nullasGyermek;
    }

    public Csomopont getEgyesGyermek()
    {
        return this.egyesGyermek;
    }

    public int getValue()
    {
        return this.value;
    }

    public void setEgyesGyermek(Csomopont node)
    {
        this.egyesGyermek = node;
    }

    public void setNullasGyermek(Csomopont node)
    {
        this.nullasGyermek = node;
    }
    
    public char getBetu()
    {
        return this.betu;
    }
}