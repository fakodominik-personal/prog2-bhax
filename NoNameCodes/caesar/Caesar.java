package caesar;

public class Caesar {

	public static void main(String[] args) throws Exception {
		Handler handler = new Handler(System.in, "out.txt");
		
		CaesarCode.setCodes();
		
		handler.process();
		handler.close();
		
	}

}
