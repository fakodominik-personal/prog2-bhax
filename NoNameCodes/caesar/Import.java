package caesar;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Import {

	private static File file = new File("ascii_formatted.txt");
	
	public static Map<Integer, String> getCodes() throws Exception
	{
		Scanner scanner;
		try
		{
			scanner = new Scanner(file);
		}catch(FileNotFoundException e)
		{
			throw e;
		}
		
		Map<Integer, String> result = new HashMap<>();
		
		String line;
		String[] parts;
		
		while(scanner.hasNextLine())
		{
			line = scanner.nextLine();
			parts = line.split("\t");
			result.put(Integer.parseInt(parts[0]), parts[1]);
		}
		
		scanner.close();
		
		return result;
		
	}
	
}
