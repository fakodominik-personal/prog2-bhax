package hu.unideb.prog2.xml;

import java.awt.Color;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import hu.unideb.prog2.model.City;

public class ColorCreator {

	public static Map<String,Color> getColors(List<City> cities)
	{
		Map<String,Color> map = new HashMap<String, Color>();
		Random random = new Random();
		
		for(City city : cities)
		{
			if(!map.containsKey(city.getState()))
			{
				map.put(city.getState(), new Color(random.nextInt(),random.nextInt(),random.nextInt()));
			}
		}
		
		
		return map;
	}
	
}
