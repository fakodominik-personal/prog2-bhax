package hu.unideb.prog2.xml;

public class Main {

	public static void main(String[] args) {
		
		Reader reader = new Reader("src/main/resources/input.xml");
		
		try
		{
			SvgMaker.make(reader.read(),"src/main/resources/out.svg");
		}catch(Exception e)
		{
			e.getStackTrace();
		}
		
	}

}
