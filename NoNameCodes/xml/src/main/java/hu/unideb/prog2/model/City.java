package hu.unideb.prog2.model;

public class City {

	private String coordinateX;
	private String coordinateY;
	private String state;
	
	public City(String coordinateX, String coordinateY, String state) {
		this.coordinateX = coordinateX;
		this.coordinateY = coordinateY;
		this.state = state;
	}
	public String getCoordinateX() {
		return coordinateX;
	}
	public String getCoordinateY() {
		return coordinateY;
	}
	public String getState() {
		return state;
	}
	
	
	
}
