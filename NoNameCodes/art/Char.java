package art;

public class Char {

	private static char[] charset = {' ','.','*','+','^','&','8','#','@'};
	
	public static char chooseChar(double g)
	{
		if (g >= 240) {
			return charset[0];
        } else if (g >= 210) {
        	return charset[1];
        } else if (g >= 190) {
        	return charset[2];
        } else if (g >= 170) {
        	return charset[3];
        } else if (g >= 120) {
        	return charset[4];
        } else if (g >= 110) {
        	return charset[5];
        } else if (g >= 80) {
        	return charset[6];
        } else if (g >= 60) {
        	return charset[7];
        } else {
        	return charset[8];
        }
        
	}
	
}
