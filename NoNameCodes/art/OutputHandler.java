package art;

import java.io.FileWriter;
import java.io.IOException;

public class OutputHandler {

	public static void write(char c, FileWriter fileWriter) throws IOException
	{
		fileWriter.write(c);
	}
	
}
