package art;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.imageio.ImageIO;

public class InputHandler implements AutoCloseable{

	private File file;
	private FileWriter fileWriter;
	private BufferedImage image;
	
	public InputHandler(String input,String output) throws IOException
	{
		this.file = new File(input);
		this.fileWriter = new FileWriter(output);
		this.image = ImageIO.read(file);
	}
	
	public void process() throws IOException
	{
		for(int i= 0; i<image.getHeight();i++)
		{
			for(int j=0;j<image.getWidth();j++)
			{
				Color color = new Color(image.getRGB(i,j));
				double pixelColor = (color.getRed() * 0.40) + (color.getGreen() * 0.30) + (color.getBlue() * 0.30);
				
				OutputHandler.write(Char.chooseChar(pixelColor),fileWriter);
				
			}
			fileWriter.append("\n");
		}
	}

	public void close() throws Exception {
		fileWriter.close();
		
	}
	
}
