package iris;

import java.util.ArrayList;

/**
 * The purpose of this class the calculate the median of the given list.
 **/
public class MedianCalculator {

	/**
	 * This calculates the median of sepal length of all species in the given list.
	 * @param species : The list containing the names and values categorized.
	 */
	public static void mediansLenght(ArrayList<IrisSpecies> species)
	{
		//In order the calculate median, we need to sort the values in ascending order
		ArrayList<Double> orderedList;
		
		//Go through of all elements in the species list, get there values, order it and calculate
		//the median of them
		for(IrisSpecies s : species)
		{
			 orderedList = s.getsLengthValues();
			 
			 //Sort them
			 orderedList = orderList(orderedList);
			 
			 if(orderedList.size() % 2 == 0)
			 {
				 //In this case we need the average of the middle values
				 s.setMediansLength( (orderedList.get(orderedList.size() /2 - 1)
						 			 + orderedList.get(orderedList.size() /2 +1)) / 2);
			 }else
			 {
				 //In this case we just need the middle value
				 s.setMediansLength( orderedList.get(orderedList.size() / 2));
			 }
		}		
	}
	/**
	 * This calculates the median of sepal width of all species in the given list.
	 * @param species : The list containing the names and values categorized.
	 */
	public static void mediansWidth(ArrayList<IrisSpecies> species)
	{
		//In order the calculate median, we need to sort the values in ascending order
		ArrayList<Double> orderedList;
		
		//Go through of all elements in the species list, get there values, order it and calculate
		//the median of them
		for(IrisSpecies s : species)
		{
			 orderedList = s.getsWidthValues();
			 
			 //Sort them
			 orderedList = orderList(orderedList);
			 
			 if(orderedList.size() % 2 == 0)
			 {
				 //In this case we need the average of the middle values
				 s.setMediansWidth( (orderedList.get(orderedList.size() /2 - 1)
						 			 + orderedList.get(orderedList.size() /2 +1)) / 2);
			 }else
			 {
				 //In this case we just need the middle value
				 s.setMediansWidth( orderedList.get(orderedList.size() / 2));
			 }
		}		
	}
	/**
	 * This calculates the median of petal length of all species in the given list.
	 * @param species : The list containing the names and values categorized.
	 */
	public static void medianpLenght(ArrayList<IrisSpecies> species)
	{
		//In order the calculate median, we need to sort the values in ascending order
		ArrayList<Double> orderedList;
		
		//Go through of all elements in the species list, get there values, order it and calculate
		//the median of them
		for(IrisSpecies s : species)
		{
			 orderedList = s.getpLengthValues();
			 
			 //Sort them
			 orderedList = orderList(orderedList);
			 
			 if(orderedList.size() % 2 == 0)
			 {
				 //In this case we need the average of the middle values
				 s.setMedianpLength( (orderedList.get(orderedList.size() /2 - 1)
						 			 + orderedList.get(orderedList.size() /2 +1)) / 2);
			 }else
			 {
				 //In this case we just need the middle value
				 s.setMedianpLength( orderedList.get(orderedList.size() / 2));
			 }
		}		
	}
	/**
	 * This calculates the median of petal width of all species in the given list.
	 * @param species : The list containing the names and values categorized.
	 */
	public static void medianpWidth(ArrayList<IrisSpecies> species)
	{
		//In order the calculate median, we need to sort the values in ascending order
		ArrayList<Double> orderedList;
		
		//Go through of all elements in the species list, get there values, order it and calculate
		//the median of them
		for(IrisSpecies s : species)
		{
			 orderedList = s.getpWidthValues();
			 
			 //Sort them
			 orderedList = orderList(orderedList);
			 
			 if(orderedList.size() % 2 == 0)
			 {
				 //In this case we need the average of the middle values
				 s.setMedianpWidth( (orderedList.get(orderedList.size() /2 - 1)
						 			 + orderedList.get(orderedList.size() /2 +1)) / 2);
			 }else
			 {
				 //In this case we just need the middle value
				 s.setMedianpWidth( orderedList.get(orderedList.size() / 2));
			 }
		}		
	}
	/**
	 * This returns a sorted list of the given list.
	 * @param values : The values that should be sorted.
	 * @return orderedList : The list sorted in ascending order.
	 */
	private static ArrayList<Double> orderList(ArrayList<Double> values)
	{
		ArrayList<Double> orderedList = values; 
		
		for(int i = 0; i<orderedList.size()-1;i++)
		 {
			 for(int j = i; j<orderedList.size();j++)
			 {
				 if(orderedList.get(i) > orderedList.get(j))
				 {
					 double temp;
					 temp = orderedList.get(i);
					 orderedList.set(i, orderedList.get(j));
					 orderedList.set(j, temp);
				 }
			 }
		 }
		
		return orderedList;
	}
}
