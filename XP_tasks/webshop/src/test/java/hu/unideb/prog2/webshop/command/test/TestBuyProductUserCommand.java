package hu.unideb.prog2.webshop.command.test;

import org.junit.Assert;
import org.junit.Test;

import hu.unideb.prog2.webshop.command.user.impl.BuyProductUserCommand;
import hu.unideb.prog2.webshop.data.DataBase;
import hu.unideb.prog2.webshop.model.Cart;
import hu.unideb.prog2.webshop.model.Product;
import hu.unideb.prog2.webshop.model.ProductType;

public class TestBuyProductUserCommand {
	
	private BuyProductUserCommand command = new BuyProductUserCommand(null,null,null);
	
	@Test
	public void testWrongProcessShouldReturnAText()
	{
		//Given
		String expected = "No need for parameters!";
		
		//When
		String result = command.process(null,null);
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void testRightProcessShouldBuyEveryElementInTheCart()
	{
		//Given
		DataBase database = new DataBase();
		ProductType pt = new ProductType("a","a","a",1);
		Product p = new Product(pt,5);
		database.addProduct(p,5);
		Cart.addToCart(p);
		boolean expected = true;
		boolean result = false;
		
		//When
		command.process(database);
		if(Cart.getCart().size() == 0)
		{
			result = true;
		}
				
		//Then
		Assert.assertEquals(expected, result);
	}
}
