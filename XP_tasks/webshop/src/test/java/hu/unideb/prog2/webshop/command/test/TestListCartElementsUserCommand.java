package hu.unideb.prog2.webshop.command.test;

import org.junit.Assert;
import org.junit.Test;

import hu.unideb.prog2.webshop.command.user.impl.ListCartElementsUserCommand;
import hu.unideb.prog2.webshop.data.DataBase;
import hu.unideb.prog2.webshop.model.Cart;
import hu.unideb.prog2.webshop.model.Product;
import hu.unideb.prog2.webshop.model.ProductType;

public class TestListCartElementsUserCommand {

	private ListCartElementsUserCommand command = new ListCartElementsUserCommand(null,null,null);
	
	@Test
	public void testWrongProcessShouldReturnAText()
	{
		//Given
		String expected = "There are no items in the cart!";
		
		//When
		String result = command.process(null,null);
		
		//Then
		Assert.assertEquals(expected,result);
	}
	
	@Test
	public void testRightProcessShouldListItemsInTheCart()
	{
		//Given
		DataBase database = new DataBase();
		Product p = new Product(new ProductType("a","a","a",3),3);
		database.addProduct(p, 3);
		String expected = p.toString() + "\n";
		
		//When
		Cart.addToCart(p);
		String result = command.process(database);
		
		//Then
		Assert.assertEquals(expected,result);
	}
	
}
