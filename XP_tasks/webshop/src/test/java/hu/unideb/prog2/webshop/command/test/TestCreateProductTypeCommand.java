package hu.unideb.prog2.webshop.command.test;

import org.junit.Assert;
import org.junit.Test;

import hu.unideb.prog2.webshop.command.impl.CreateProductTypeCommand;
import hu.unideb.prog2.webshop.data.DataBase;
import hu.unideb.prog2.webshop.model.ProductType;

public class TestCreateProductTypeCommand {

	private CreateProductTypeCommand command = new CreateProductTypeCommand(null, null, null);
	
	@Test
	public void testWrongProcessShouldReturnAText()
	{
		//Given
		String expected = "Need parameters!";
		
		//When
		String result = command.process(null);
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void testRightProcessShouldAddElementToTheDatabaseWhenGivenTheData()
	{
		//Given
		DataBase database = new DataBase();
		String[] parameters = {"a","a","a","1"};
		ProductType expected = new ProductType("a","a","a",1);
				
		//When
		command.process(database, parameters);
		ProductType result = database.getProductType(0);
				
		//Then
		Assert.assertEquals(expected, result);
	}
	
}
