package hu.unideb.prog2.webshop.command.test;

import org.junit.Assert;
import org.junit.Test;

import hu.unideb.prog2.webshop.command.impl.RemoveProductTypeCommand;
import hu.unideb.prog2.webshop.data.DataBase;
import hu.unideb.prog2.webshop.model.ProductType;

public class TestRemoveProductTypeCommand {

private RemoveProductTypeCommand command = new RemoveProductTypeCommand(null,null,null);
	
	@Test
	public void testWrongProcessShouldReturnAText()
	{
		//Given
		String expected = "Need parameters!";
		
		//When
		String result = command.process(null);
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void testRightProcessShouldRemoveElementFromTheDatabaseWhenGivenTheID()
	{
		//Given
		String[] parameters = {"0"};
		DataBase database = new DataBase();
		ProductType pt = new ProductType("a","a","a",1);
		database.addProductType(pt);
		int expected = 0;
				
		//When
		command.process(database, parameters);
		int result = database.getProductList().size();
				
		//Then
		Assert.assertEquals(expected, result);
	}
	
}
