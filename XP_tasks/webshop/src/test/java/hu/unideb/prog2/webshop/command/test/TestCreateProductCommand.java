package hu.unideb.prog2.webshop.command.test;

import org.junit.Assert;
import org.junit.Test;

import hu.unideb.prog2.webshop.command.impl.CreateProductCommand;
import hu.unideb.prog2.webshop.data.DataBase;
import hu.unideb.prog2.webshop.model.Product;
import hu.unideb.prog2.webshop.model.ProductType;

public class TestCreateProductCommand {

	private CreateProductCommand command = new CreateProductCommand("admin","product","create");
	
	@Test
	public void testWrongProcessShouldReturnAText()
	{
		//Given
		String expected = "Need parameters!";
		
		//When
		String result = command.process(null);
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void testRightProcessShouldAddElementToTheDatabaseWhenGivenTheData()
	{
		//Given
		DataBase database = new DataBase();
		String[] parameters = {"0","23434"};
		ProductType pt = new ProductType("a","a","a",1);
		database.addProductType(pt);
		Product expected = new Product(pt,23434);
				
		//When
		command.process(database, parameters);
		Product result = database.getProduct(0);
				
		//Then
		Assert.assertEquals(expected, result);
	}
}
