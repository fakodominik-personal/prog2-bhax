package hu.unideb.prog2.webshop.command.test;

import org.junit.Assert;
import org.junit.Test;

import hu.unideb.prog2.webshop.command.impl.ImportProductCommand;
import hu.unideb.prog2.webshop.data.DataBase;

public class TestImportProductCommand {

private ImportProductCommand command = new ImportProductCommand(null,null,null);
	
	@Test
	public void testWrongProcessShouldReturnAText()
	{
		//Given
		String expected = "Need file name as parameter!";
		
		//When
		String result = command.process(null);
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void testRightProcessShouldAddEveryElementFromTheFileToTheDatabase()
	{
		//Given
		DataBase database = new DataBase();
		String[] parameters = {"src/test/resources/testProduct.csv"};
		int expected = 1;
		
		//When
		command.process(database, parameters);
		int result = database.getProductList().size();
		
		//Then
		Assert.assertEquals(expected, result);
	}
	
}
