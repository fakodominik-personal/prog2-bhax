package hu.unideb.prog2.webshop.command.user.impl;

import hu.unideb.prog2.webshop.command.AbstractCommand;
import hu.unideb.prog2.webshop.data.DataBase;
import hu.unideb.prog2.webshop.model.Cart;

public class ListCartElementsUserCommand extends AbstractCommand{

	public ListCartElementsUserCommand(String userType, String entityType, String action) {
		super(userType, entityType, action);
	}

	@Override
	public String process(DataBase database) {
		return Cart.listItems();
	}

	@Override
	public String process(DataBase database, String[] parameters) {
		return "There are no items in the cart!";
	}

}
