package hu.unideb.prog2.webshop.data;

import java.util.Collection;
import java.util.HashMap;

import hu.unideb.prog2.webshop.model.Product;
import hu.unideb.prog2.webshop.model.ProductType;

public class DataBase {
	
	private HashMap<Integer,Product> productList;
	private HashMap<Integer,ProductType> productTypeList;
	private int productTypeCounter;
	
	public DataBase() {
		this.productList = new HashMap<>();
		this.productTypeList = new HashMap<>();
		productTypeCounter = 0;
	}
	
	public void addProduct(Product product,int key)
	{
		productList.put(key,product);
	}
	public void addProductType(ProductType productType)
	{
		productTypeList.put(productTypeCounter,productType);
		productType.setId(productTypeCounter);
		productTypeCounter++;
		
	}
	public Product getProduct(int key)
	{
		return productList.get(key);
	}
	public ProductType getProductType(int key)
	{
		return productTypeList.get(key);
	}
	public void removeProduct(int key)
	{
		productList.remove(key);
	}
	public void removeProductType(int key)
	{
		if(productList.containsKey(key))
		{
			removeProduct(key);
		}
		productTypeList.remove(key);
		
	}
	public Collection<Product> getProductList()
	{
		return productList.values();
	}
	public Collection<ProductType> getProductTypeList()
	{
		return productTypeList.values();
	}

}
