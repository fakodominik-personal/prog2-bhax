package hu.unideb.prog2.webshop.command.impl;

import hu.unideb.prog2.webshop.command.AbstractCommand;
import hu.unideb.prog2.webshop.data.DataBase;
import hu.unideb.prog2.webshop.model.Product;

public class CreateProductCommand extends AbstractCommand{

	public CreateProductCommand(String userType, String entityType, String action) {
		super(userType, entityType, action);
	}

	@Override
	public String process(DataBase database){
		return "Need parameters!";
	}
	
	public String process(DataBase database,String[] parameters)
	{
		
		if(database.getProductType(Integer.parseInt(parameters[0])) == null)
		{
			throw new IllegalArgumentException("No product type with that id!");
		}
		
		try
		{
			Product p = new Product(database.getProductType(Integer.parseInt(parameters[0])),Long.parseLong(parameters[1]));
			database.addProduct(p,database.getProductType(Integer.parseInt(parameters[0])).getId());
			return "Product created succesfully : \n" + p.toString() + "\n";
		}catch(Exception e)
		{
			throw new ClassCastException("Wrong parameters were given!");
		}
		
	}

}
