package hu.unideb.prog2.webshop.model;

import java.util.ArrayList;

public class Cart {

	private static ArrayList<Product> cart = new ArrayList<>();
	
	public static void addToCart(Product pt)
	{
		cart.add(pt);
	}
	
	public static String listItems()
	{
		String result = "";
		for(Product p : cart)
		{
			result += p.toString() + "\n";
		}
		
		return result;
	}

	public static ArrayList<Product> getCart() {
		return cart;
	}
	
	public static void emptyCart()
	{
		cart.clear();
	}
	
}
