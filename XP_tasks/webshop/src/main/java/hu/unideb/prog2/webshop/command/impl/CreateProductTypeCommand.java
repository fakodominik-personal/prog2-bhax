package hu.unideb.prog2.webshop.command.impl;

import hu.unideb.prog2.webshop.command.AbstractCommand;
import hu.unideb.prog2.webshop.data.DataBase;
import hu.unideb.prog2.webshop.model.ProductType;

public class CreateProductTypeCommand extends AbstractCommand{

	public CreateProductTypeCommand(String userType, String entityType, String action) {
		super(userType, entityType, action);
	}

	@Override
	public String process(DataBase database) {		
		return "Need parameters!";
	}

	@Override
	public String process(DataBase database, String[] parameters){
		try
		{
			ProductType pt = new ProductType(parameters[0], parameters[1],parameters[2],Long.parseLong(parameters[3]));
			database.addProductType(pt);
			return "Product Type was successfully created :\n" + pt.toString() + "\n";
		}catch(Exception e)
		{
			throw new IllegalArgumentException("Wrong arguments were given!");
		}
		
		
	}

}
