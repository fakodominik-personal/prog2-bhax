package hu.unideb.prog2.webshop.command.impl;

import java.util.ArrayList;

import hu.unideb.prog2.webshop.command.AbstractCommand;
import hu.unideb.prog2.webshop.data.DataBase;
import hu.unideb.prog2.webshop.model.Product;

public class ListProductCommand extends AbstractCommand{

	public ListProductCommand(String userType, String entityType, String action) {
		super(userType, entityType, action);
	}

	@Override
	public String process(DataBase database) {
		String result = "";
		ArrayList<Product> array = new ArrayList<>(database.getProductList());
		for(int i=0;i<array.size();++i)
		{
			result += array.get(i).toString() + "\n";
		}
		return result;
	}

	@Override
	public String process(DataBase database, String[] parameters) {
		return "No need for parameters!";
	}

}
