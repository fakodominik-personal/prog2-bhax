package hu.unideb.prog2.webshop.command.impl;

import hu.unideb.prog2.webshop.command.AbstractCommand;
import hu.unideb.prog2.webshop.data.DataBase;

public class BaseCommand extends AbstractCommand{

	public BaseCommand(String userType, String entityType, String action) {
		super(userType, entityType, action);
	}

	@Override
	public String process(DataBase database) {
		return null;
	}

	@Override
	public String process(DataBase database, String[] parameters) {
		return null;
	}

}
