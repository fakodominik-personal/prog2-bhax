package hu.unideb.prog2.webshop.command;

import hu.unideb.prog2.webshop.data.DataBase;

public interface Command {

	String process(DataBase database);
	
	String process(DataBase database,String[] parameters);
	
}
