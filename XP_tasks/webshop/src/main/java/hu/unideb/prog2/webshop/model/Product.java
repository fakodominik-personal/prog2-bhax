package hu.unideb.prog2.webshop.model;

public class Product {

	private ProductType productType;
	private long serialNum;
	private int id;
	
	public Product(ProductType productType, long serialNum) {
		super();
		this.productType = productType;
		this.serialNum = serialNum;
		this.id = productType.getId();
	}

	public ProductType getProductType() {
		return productType;
	}

	public long getSerialNum() {
		return serialNum;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", productType=" + productType + ", serialNum=" + serialNum + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((productType == null) ? 0 : productType.hashCode());
		result = prime * result + (int) (serialNum ^ (serialNum >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (productType == null) {
			if (other.productType != null)
				return false;
		} else if (!productType.equals(other.productType))
			return false;
		if (serialNum != other.serialNum)
			return false;
		return true;
	}
	
	
}
