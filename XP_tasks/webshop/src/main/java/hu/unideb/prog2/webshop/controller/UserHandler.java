package hu.unideb.prog2.webshop.controller;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;
import java.util.Set;

import hu.unideb.prog2.webshop.command.Command;
import hu.unideb.prog2.webshop.command.impl.BaseCommand;
import hu.unideb.prog2.webshop.data.DataBase;

public class UserHandler implements AutoCloseable{
	
	private InputStream input;
	private Scanner scanner;
	private OutputStream output;
	private Set<Command> commands;
	private DataBase database;

	public UserHandler(InputStream input, OutputStream output, Set<Command> commands, DataBase database) throws Exception
	{
		
		if(input == null)
		{
			throw new Exception("Input stream needed!");
		}
		if(output == null)
		{
			throw new Exception("Output stream needed!");
		}
		
		this.input = input;
		this.output = output;
		this.commands = commands;
		this.database = database;
	}
	
	public void process() throws Exception
	{	
		scanner = new Scanner(input);
		String line;
		String[] parts;
		line = scanner.nextLine();
		if(line.equals("exit"))
		{
			closeApplication();
		}
		while(!line.equals("exit"))
		{
			 parts = line.split(" ");
			 if(parts.length < 3)
			 {
				 throw new Exception("Unknown command!");
			 }
			Command command = new BaseCommand(parts[0], parts[1], parts[2]);
			for(Command c : commands)
			{
				if(command.equals(c))
				{
					command = c;
				}
			}
			if(command.getClass() == (new BaseCommand("","","").getClass()))
			{
				throw new Exception("Unknown command!");
			}else
			{
				if(parts.length > 3)
				{
					String[] parameters = new String[parts.length-3];
					for(int i=3;i<parts.length;i++)
					{
						parameters[i-3] = parts[i]; 
					}
					try
					{
						output.write(command.process(database,parameters).getBytes());
					}catch(IllegalArgumentException e)
					{
						throw e;
					}catch(ClassCastException e)
					{
						throw e;
					}
					catch(Exception e)
					{
						throw new Exception("Wrong parameters were given!");
					}
				}else
				{
					try
					{
					output.write(command.process(database).getBytes());
					}catch(Exception e)
					{
						throw new Exception("Wrong parameters were given!");
					}
				}
			}
			line = scanner.nextLine();	
			if(line.equals("exit"))
			{
				System.exit(0);
			}
		}
		
	}

	private void closeApplication()
	{
		//Saving or anything before closing
		
		System.exit(0);
	}
	
	
	@Override
	public void close() throws Exception {
		scanner.close();
		output.close();
		
	}
	
	
}
