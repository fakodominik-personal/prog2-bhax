package hu.unideb.prog2.webshop.command.user.impl;

import java.util.ArrayList;

import hu.unideb.prog2.webshop.command.AbstractCommand;
import hu.unideb.prog2.webshop.data.DataBase;
import hu.unideb.prog2.webshop.model.ProductType;

public class ListProductTypeUserCommand extends AbstractCommand{

	public ListProductTypeUserCommand(String userType, String entityType, String action) {
		super(userType, entityType, action);
	}

	@Override
	public String process(DataBase database) {
		ArrayList<ProductType> array = new ArrayList<>(database.getProductTypeList());
		String result = "";
		for(ProductType pt : array)
		{
			result += pt.toString() + "\n";
		}
		return result;
	}

	@Override
	public String process(DataBase database, String[] parameters) {
		return "No need for parameters!";
	}

}
