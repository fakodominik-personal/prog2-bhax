package aspectj_runtime;

public aspect Counter {

	pointcut count() : execution(* Main.addToList());
	
	private long start;
	private long end;
	
	before() : count()
	{
		start = System.currentTimeMillis();
	}
	after() : count()
	{
		end = System.currentTimeMillis();
		System.out.println("Time spent during the method : " + (end-start) + "ms");
	}
	
}
