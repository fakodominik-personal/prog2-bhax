#include <iostream>
#include <dirent.h>
#include <cstring>

using namespace std;

void listdir(string path);
string checkPathString(string strPath);

string checkPathString(string strPath)
{
   if(strPath[strPath.length()-1] == '/')
   {
      strPath = strPath.substr(0,strPath.length()-1);
   }
   return strPath;
}




void listdir(string path)
{
   struct dirent *dir;
   DIR *dp = opendir(path.c_str());
   if(dp)
   {
      while((dir = readdir(dp)) != NULL)
      {
         string strPath = "";
         if(strcmp(dir->d_name,".") == 0 || strcmp(dir->d_name,"..") == 0)
         {
            continue;
         }
         if(dir->d_type == DT_DIR)
         {
            path = checkPathString(path);
            strPath = path + "/" + dir->d_name;
            listdir(strPath);
         }else
         {
            path = checkPathString(path);
            strPath = dir->d_name;
            cout<<strPath<<endl;
         }
      }
      closedir(dp);
   }


}

int main(int argc, char*argv[])
{
   listdir(argv[1]);
   return 0;
}