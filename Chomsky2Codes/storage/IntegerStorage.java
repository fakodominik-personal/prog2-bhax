package storage;

public class IntegerStorage
{
	private int maxSize;
	private int currentSize;
	private int[] storage;

	public IntegerStorage(int num)
	{
		this.maxSize = num;
		storage = new int[maxSize];
		this.currentSize = 0;
	}
	public void add(int value)
	{
		if(currentSize == maxSize)
		{
			System.out.println("The storage is full");
		}else
		{
			storage[currentSize] = value;
			currentSize++;
		}
	}
	public int[] sort()
	{
		int[] currentStorage = storage;
		for(int i=0;i<currentSize;i++)
		{
			for(int j=0;j<currentSize-1;j++)
			{
				if(currentStorage[i] < currentStorage[j])
				{
					int temp = currentStorage[i];
					currentStorage[i] = currentStorage[j];
					currentStorage[j] = temp;
				}
			}
		}
		return currentStorage;
	}
	public boolean contains(int value)
	{
		int [] currentStorage = sort();
		int start = 0;
		int end = currentSize;
		return binarySearch(start,end,currentStorage, value);
	}
	private boolean binarySearch(int start, int end,int[] array, int value)
	{
		int mid = (start+end) / 2;
		int length = end - start;
		if(length <= 0)
		{
			return false;
		}else if(array[mid] == value)
		{
			return true;
		}else if(array[mid] < value)
		{
			return binarySearch(start,mid + 1,array,value);
		}else if(array[mid] > value)
		{
			return binarySearch(mid,end - 1,array,value);
		}
		return false;
	}
}