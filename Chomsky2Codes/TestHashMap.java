package hu.unideb.prog2;

import static org.junit.Assert.assertEquals;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Test;

public class TestHashMap {

	public HashMap<String,String> map = new HashMap<>();
	
	@Test
	public void testPutShouldPutTheGivenElementToTheMap()
	{
		//Given
			String key = "key";
			String value = "value";
			int expected = 1;
		//When
		map.put(key, value);
		map.put(key, value);
		int result = map.size();
		//Then
		assertEquals(expected,result);
	}
	@Test
	public void testSizeSouldReturnTheCorrectNumberOfTheMapsSize()
	{
		//Given
		int expected = 0;
		
		//When
		int result = map.size();
		//Then
		assertEquals(expected,result);
	}
	@Test
	public void testIsEmptyShouldReturnTrueWhenTheMapIsEmpty()
	{
		//Given
		boolean expected = true;
		//When
		boolean result = map.isEmpty();
		//Then
		assertEquals(expected,result);
	}
	@Test
	public void testContainsKeyShouldReturnTrueWhenTheMapContainsTheGivenKey()
	{
		//Given
		String key = "key";
		String value = "value";
		boolean expected = true;
		map.put(key, value);
		//When
		boolean result = map.containsKey(key);
		//Then
		assertEquals(expected,result);
	}
	@Test
	public void testGetShouldReturnTheCurrentValueOfTheNodeWhenGetTheKey()
	{
		//Given
		String key = "key";
		String value = "value";
		String expected = "value";
		map.put(key, value);
		//When
		String result = map.get(key);
		//Then
		assertEquals(expected,result);
	}
	@Test
	public void testContainsValueShouldReturnTrueWhenTheMapContainsTheGivenValue()
	{
		//Given
		String key = "key";
		String value = "value";
		boolean expected = true;
		map.put(key, value);
		//When
		boolean result = map.containsValue(value);
		//Then
		assertEquals(expected,result);
	}
	@Test
	public void testRemoveShouldRemoveOneElementFromTheMap()
	{
		//Given
		String key = "key";
		String value = "value";
		int expected = 1;
		map.put(key, value);
		map.put("key2", value);
		
		//When
		map.remove(key);
		int result = map.size();
		
		//Then
		assertEquals(expected,result);
	}
	@Test
	public void testClearShouldRemoveAllElementsInTheMap()
	{
		//Given
		String key = "key";
		String value = "value";
		map.put(key, value);
		map.put("key2", value);
		map.put("key3", value);
		int expected = 0;
		//When
		map.clear();
		int result = map.size();
		//Then
		assertEquals(expected,result);
	}
	@Test
	public void testPutAllShouldAppendEveryElementFromTheGivenMapToTheCurrentMap()
	{
		//Given
		String key = "key";
		String value = "value";
		map.put(key, value);
		
		Map<String,String> anotherMap = new HashMap<>();
		anotherMap.put("key2", value);
		anotherMap.put("key3", value);
		anotherMap.put("key4", value);
		
		int expected = 4;
		
		//When
		map.putAll(anotherMap);
		int result = map.size();
		
		//Then
		assertEquals(expected,result);
	}
	@Test
	public void testKeySetShouldReturnASetContainingTheKeysOfTheElementsInTheMap()
	{
		//Given
		Set<String> set = new HashSet<String>();
		String key = "key";
		String value = "value";
		map.put(key, value);
		map.put("key2", value);
		map.put("key3", value);
		int expected = 3;
		//When
		set = map.keySet();
		int result = set.size();
		//Then
		assertEquals(expected,result);
		
	}
	@Test
	public void testValuesShouldReturnACollectionContainingTheValuessOfTheElementsInTheMap()
	{
		//Given
		Collection<String> collection = new ArrayList<String>();
		String key = "key";
		String value = "value";
		map.put(key, value);
		map.put("key2", value);
		map.put("key3", value);
		int expected = 3;
		//When
		collection = map.values();
		int result = collection.size();
		//Then
		assertEquals(expected,result);
		
	}
	@Test
	public void testEntrySetShouldReturnASetOfEntrysFromTheCurrentMap()
	{
		//Given
		Set<Entry<String,String>> set = new HashSet<>();
		Set<Entry<String,String>> setResult = new HashSet<>();
		Entry<String,String> entry = new AbstractMap.SimpleEntry<>("key","value");
		set.add(entry);
		String key = "key";
		String value = "value";
		map.put(key, value);
		boolean expected = true;
		//When
		setResult = map.entrySet();
		boolean result = setResult.equals(set);
		//Then
		assertEquals(expected,result);
		
	}
}
