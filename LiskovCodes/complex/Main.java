//A forráskód Lámfalusi Csaba tulajdona, jelenleg bemutatásként 
//használom egy másik feladathoz!
package complex;

public class Main {

	public static void main(String[] args) {
		IntegerCollection collection = new IntegerCollection(3);
		collection.add(0);
		collection.add(2);
		collection.add(1);
		System.out.println(collection);
		collection.sort();
		System.out.println(collection);
		System.out.println(collection.contains(0));
		System.out.println("The cyclomatic complexity of the contains function when"
			+ " 0 is the parameter of the function is : "
			+ collection.getComplexity());
	}

}