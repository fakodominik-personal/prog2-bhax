package principle;

import java.util.ArrayList;

public class Main
{
	public static void main(String[] args)
	{
		ArrayList<Bird> birds = new ArrayList<Bird>();
		birds.add(new Eagle("Eagle"));
		birds.add(new Penguin("Penguin"));

		for(Bird b : birds)
		{
			b.fly(b);
		}
	}
}