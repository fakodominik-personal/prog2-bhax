package principle;

class Bird{
	protected String name;

	public void fly(Bird b)
	{
		System.out.println("The " + b.getName() + " is flying!");

	}

	public String getName()
	{
		return this.name;
	}
} 