package car;

public class Supercar extends Car
{
	public Supercar()
	{
		System.out.println("Supercar's constructor");
	}

	@Override
	public void start()
	{
		System.out.println("Supercar's start()");
	}
}