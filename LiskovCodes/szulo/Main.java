package szulo;

public class Main
{
	public static void main(String[] args)
	{
		Szulo szulo = new Szulo("Tarka Kft", 50);
		Gyerek gyerek = new Gyerek("Boci Kft", 20);

		System.out.println("A szulo osztaly neve : " + szulo.getName()
		 + " es a dolgozok szama: " + szulo.getNumberOfWorkers());

		System.out.println("A gyerek osztaly neve : " + gyerek.getName()
		 + " es a dolgozok szama: " + gyerek.getNumberOfWorkers());
	}
}