package Motorcycle;


/**
* @generated
*/
public class Motorcycle implements Vehicle {
    
    /**
    * @generated
    */
    private Integer numberOfTires;
    
    /**
    * @generated
    */
    private String nameOFTheOwner;
    
    /**
    * @generated
    */
    private String registrationNumber;
    
    /**
    * @generated
    */
    private String model;
    
    /**
    * @generated
    */
    private Date dateOfProducion;
    
    
    
    /**
    * @generated
    */
    protected Integer getNumberOfTires() {
        return this.numberOfTires;
    }
    
    /**
    * @generated
    */
    protected Integer setNumberOfTires(Integer numberOfTires) {
        this.numberOfTires = numberOfTires;
    }
    
    /**
    * @generated
    */
    protected String getNameOFTheOwner() {
        return this.nameOFTheOwner;
    }
    
    /**
    * @generated
    */
    protected String setNameOFTheOwner(String nameOFTheOwner) {
        this.nameOFTheOwner = nameOFTheOwner;
    }
    
    /**
    * @generated
    */
    protected String getRegistrationNumber() {
        return this.registrationNumber;
    }
    
    /**
    * @generated
    */
    protected String setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }
    
    /**
    * @generated
    */
    protected String getModel() {
        return this.model;
    }
    
    /**
    * @generated
    */
    protected String setModel(String model) {
        this.model = model;
    }
    
    /**
    * @generated
    */
    protected Date getDateOfProducion() {
        return this.dateOfProducion;
    }
    
    /**
    * @generated
    */
    protected Date setDateOfProducion(Date dateOfProducion) {
        this.dateOfProducion = dateOfProducion;
    }
    
    

    //                          Operations                                  
    
    /**
    * @generated
    */
    public getMotorcycle() {
        //TODO
    }
    
}
