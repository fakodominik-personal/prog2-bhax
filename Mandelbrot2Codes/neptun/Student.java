package neptun;

public class Student extends Person{
	
	public Student(String name, int age, String neptun)
	{
		this.name = name;
		this.age = age;
		this.neptunId = neptun;
		this.balance = 0;
	}
	public void courseRegistration(Course course)
	{
		if(/*The given course is in the course library*/true)
		{
			this.courses.add(course);
		}
	}
	public void payTheBill(Bill bill)
	{
		if(/*The Bill id is exist*/true)
		{
			if(bill.amount <= this.balance)
			{
				bill.payBill();
			}
		}
	}
}
