package neptun;

import java.util.Date;

public class Teacher extends Person{

	public Course createCourse(Date date, int maxNum, String id)
	{
		return new Course(date, maxNum, this, id);
	}
	public void openCourse(Course course)
	{
		if(/*The given course is in the database*/true)
		{
			course.open();
		}
	}
	public void registerAttendance(Course course, Student student)
	{
		if(/*The given course is in the database*/true)
		{
			course.addToAttendance(student);
		}
	}
	public boolean hasCourses()
	{
		if(courses.isEmpty())
		{
			return false;
		}
		return true;
	}
	public Teacher(String name, int age, String neptun)
	{
		this.name = name;
		this.age = age;
		this.neptunId = neptun;
		this.balance = 0;
	}
	
}
