package neptun;

import java.util.ArrayList;
import java.util.List;

public abstract class Person {

	protected String name;
	protected int age;
	protected String neptunId;
	protected int balance;
	protected int numberOfCourses;
	protected List<Course> courses = new ArrayList<>();
	
	public String getName()
	{
		return this.name;
	}
	public int gatAge()
	{
		return this.age;
	}
	public String getNeptunId() {
		return neptunId;
	}
	public int getBalance() {
		return balance;
	}
	public int getNumberOfCourses() {
		return numberOfCourses;
	}
	public List<Course> getCourses() {
		return courses;
	}
	public void addToBalance(int amount)
	{
		if(/*the payment was successful*/true)
		{
			this.balance+=amount;
		}
	}
}
