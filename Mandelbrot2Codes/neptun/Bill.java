package neptun;

import java.util.Date;

public class Bill {

		public int amount;
		public Date expireDate;
		public String name;
		public String id;
		private boolean done;
		
		public Bill(String id, int amount, Date expDate, String name)
		{
			this.amount = amount;
			this.expireDate = expDate;
			this.name = name;
			this.id = id;
			done = false;
		}
		public void payBill()
		{
			done = true;
		}
}
