package neptun;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Course extends Subject{

		private Date date;
		private int maxNumberOfStudents;
		private Teacher teacher;
		private String comment;
		private String id;
		private List<Student> attendedStudents = new ArrayList<>();
		private List<Student> students = new ArrayList<>();
		private boolean isOpen;
		
		public Date getDate()
		{
			return this.date;
		}
		public Teacher getTeacher()
		{
			return this.teacher;
		}
		public int getMaxStudents()
		{
			return this.maxNumberOfStudents;
		}
		public List<Student> getStudents()
		{
			return students;
		}
		public void addToStudents(Student student)
		{
			students.add(student);
		}
		public void setComment(String comm)
		{
			this.comment = comm;
		}
		public void addToAttendance(Student student)
		{
			attendedStudents.add(student);
		}
		public Course(Date date, int maxNum, Teacher teacher, String id)
		{
			this.date = date;
			this.maxNumberOfStudents = maxNum;
			this.teacher = teacher;
			this.id = id;
			isOpen = false;
		}
		public void open()
		{
			isOpen = true;
		}
}
