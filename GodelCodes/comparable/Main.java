package comparable;

public class Main {

	public static void main(String[] args) {
		Student joe = new Student("Joe",18,112);
		Student bill = new Student("Bill",19,110);
		
		int result = joe.compareTo(bill);
		
		if(result == -1)
		{
			System.out.println("Joe is the higher.");
		}else if(result == 1)
		{
			System.out.println("Bill is the higher.");
		}else
		{
			System.out.println("They are equal.");
		}

	}

}
