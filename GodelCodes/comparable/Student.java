package comparable;

public class Student implements Comparable<Student>{

	private String name;
	private int age;
	private int id;
	
	public Student(String name, int age,int id)
	{
		this.name = name;
		this.age = age;
		this.id = id;
	}

	@Override
	public int compareTo(Student s) {
		if(this.id < s.id)
		{
			return 1;
		}else if(this.id > s.id)
		{
			return -1;
		}else
		{
			return 0;
		}
	}
	
	//other stuff
	
}
