package refactoring;

public class OldStringBuilder {

	public static String builder(String[] parts)
	{
		String done = "";
		for(String s : parts)
		{
			done += s;
		}
		return done;
	}
}
