import java.util.Arrays;
import java.util.stream.*;

public class MatrixMultiplication
{
	public static void main(String[] args)
	{
    double[][] m1 = { { 9, 10 }, { 20, 2 }, { 0, 9 } };
    double[][] m2 = { { 4, 7 }, { 9, 4 } };

    double[][] result = Arrays.stream(m1).map(r -> 	//Convert array to stream
        IntStream.range(0, m2[0].length).mapToDouble(i -> //Convert the elements into an array
            IntStream.range(0, m2.length).mapToDouble(j -> r[j] * m2[j][i]).sum() //Multiplying
    ).toArray()).toArray(double[][]::new); //Convert to matrix

    System.out.println(Arrays.deepToString(result));
	}
}